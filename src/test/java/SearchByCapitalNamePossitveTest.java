import io.restassured.response.Response;

import java.util.Scanner;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SearchByCapitalNamePossitveTest {


    /**
     * use the scanner to pass the right country capital name and validate
     * the connection is successful ang see the 200 message
     */

    public static void main(String[] args) {
        String baseURI = "https://restcountries.eu/rest/v2/";
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the capital name : ");
        String capitalName = scanner.nextLine().toLowerCase();
        System.out.println("The Capital name you entered is : " + capitalName);

        Response response = given()
                .pathParam("capital", capitalName)
                .get(baseURI + "capital/{capital}").prettyPeek();
        response.then().assertThat().statusCode(200);
        assertEquals(response.jsonPath().getString("[0].capital").toLowerCase(), capitalName);
    }

}




