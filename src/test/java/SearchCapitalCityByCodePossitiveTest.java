import io.restassured.response.Response;

import java.util.Scanner;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

public class SearchCapitalCityByCodePossitiveTest {

    public static void main(String[] args) {

        /**
         * use the scanner to pass the right country capital code and validate
         * the connection is successful ang see the 200 message
         */

        String baseURI = "https://restcountries.eu/rest/v2/";
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the capital code : ");

        String Code = scanner.nextLine().toUpperCase();
        System.out.println("You entered country code is : " + Code);

        Response response = given()
                .pathParam("alpha", Code)
                .get(baseURI + "alpha/{alpha}").prettyPeek();
        response.then().assertThat().statusCode(200)
                .body("alpha2Code", is(Code));
    }


}


