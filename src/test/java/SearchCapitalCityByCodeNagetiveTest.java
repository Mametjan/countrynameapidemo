import io.restassured.response.Response;

import java.util.Scanner;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

public class SearchCapitalCityByCodeNagetiveTest {

    /**
     * use the scanner to pass the wrong country capital name and validate
     * that you will see 404 (page not found) message .
     */


    public static void main(String[] args) {

        String baseURI = "https://restcountries.eu/rest/v2/";
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the capital code : ");
        //you pass wrong capital code
        String Code = scanner.nextLine().toUpperCase();
        System.out.println("You entered country code is : " + Code);

        Response response = given()
                .pathParam("alpha", Code)
                .get(baseURI + "alpha/{alpha}").prettyPeek();
        response.then().assertThat().statusCode(404)
                .body("message", is("Not Found"));
    }


}
