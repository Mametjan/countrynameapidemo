import io.restassured.response.Response;

import java.util.Scanner;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

public class SearchByCapitalNameNagetiveTest {


    /**
     * use the scanner to pass the wrong country capital name and validate
     * that you will see 404 (page not found) message .
     */


    public static void main(String[] args) {


        String baseURI = "https://restcountries.eu/rest/v2/";
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the capital name : ");
        //you pass wrong capital name
        String capitalName = scanner.nextLine().toLowerCase();
        System.out.println("The Capital name you entered is : " + capitalName);

        Response response = given()
                .pathParam("capital", capitalName)
                .get(baseURI + "capital/{capital}").prettyPeek();
        response.then().assertThat().statusCode(404)
                .body("message", is("Not Found"));

    }
}
